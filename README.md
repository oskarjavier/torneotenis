
### Run docker

docker-compose up -d --build

### Stop docker

docker-compose down

### URL de los Servicios 

### Arranque de aplicacion local puerto 8095 en docker 8095
### Swagger:
http://localhost:8095/swagger-ui/index.html#/confrontation-controller

### uso:
## Usar Swagger:
1) url:
/torneo/tournaments/feminine


## Pruebas:
## JSON Test:
[
{
"ability": 100,
"name": "string1",
"reactionTime": 0,
"sex": "F"
},
{
"ability": 50,
"name": "string2",
"reactionTime": 10,
"sex": "F"
},
{
"ability": 40,
"name": "string3",
"reactionTime": 20,
"sex": "F"
},
{
"ability": 60,
"name": "string4",
"reactionTime": 10,
"sex": "F"
},
{
"ability": 70,
"name": "string5",
"reactionTime": 20,
"sex": "F"
},
{
"ability": 10,
"name": "string6",
"reactionTime": 20,
"sex": "F"
},
{
"ability": 60,
"name": "string7",
"reactionTime": 15,
"sex": "F"
},
{
"ability": 30,
"name": "string8",
"reactionTime": 9,
"sex": "F"
},
{
"ability": 40,
"name": "string9",
"reactionTime": 10,
"sex": "F"
},
{
"ability": 60,
"name": "string10",
"reactionTime": 8,
"sex": "F"
},
{
"ability": 40,
"name": "string11",
"reactionTime": 11,
"sex": "F"
},
{
"ability": 80,
"name": "string12",
"reactionTime": 10,
"sex": "F"
},
{
"ability": 70,
"name": "string13",
"reactionTime": 20,
"sex": "F"
},
{
"ability": 30,
"name": "string14",
"reactionTime": 5,
"sex": "F"
},
{
"ability": 40,
"name": "string15",
"reactionTime": 8,
"sex": "F"
},
{
"ability": 60,
"name": "string16",
"reactionTime": 10,
"sex": "F"
}
]

2) Luego use esta URL para que se corra el torneo:
   /torneo/tournaments/play/{idTournament}
  
### URL AWS:
http://44.202.159.161:8095/swagger-ui/index.html