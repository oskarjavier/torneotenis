package com.geopago.torneo.repository;

import com.geopago.torneo.model.Confrontation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfrontationRepository extends JpaRepository<Confrontation, Long> {
}
