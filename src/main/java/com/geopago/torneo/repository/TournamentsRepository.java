package com.geopago.torneo.repository;

import com.geopago.torneo.model.Confrontation;
import com.geopago.torneo.model.Phase;
import com.geopago.torneo.model.Tournaments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TournamentsRepository extends JpaRepository<Tournaments, Long> {

    @Query("SELECT pha FROM Tournaments t, IN(t.phases) as pha WHERE pha.active = true AND t.id = :id")
    Optional<List<Phase>> findTournamentGetConfrontation(@Param("id") Long id);

    @Query("SELECT pha FROM Tournaments t, IN(t.phases) as pha WHERE pha.sec = :sec AND t.id = :id")
    Optional<List<Phase>> findPhaseByTournamentAndSecGetConfrontation(@Param("id") Long id, @Param("sec") Integer sec);
}
