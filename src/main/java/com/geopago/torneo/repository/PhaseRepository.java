package com.geopago.torneo.repository;

import com.geopago.torneo.model.Phase;
import com.geopago.torneo.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhaseRepository extends JpaRepository<Phase, Long> {
}
