package com.geopago.torneo.service;

import com.geopago.torneo.model.Confrontation;
import com.geopago.torneo.model.Phase;
import com.geopago.torneo.model.Tournaments;
import com.geopago.torneo.repository.ConfrontationRepository;
import com.geopago.torneo.repository.PhaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhaseService {
    @Autowired
    private PhaseRepository phaseRepository;
    @Autowired
    private ConfrontationRepository confrontationRepository;

    public Phase addPhase(Boolean active, String name, Tournaments tournaments, List<Confrontation> confrontations, Integer sec){
        Phase phase=new Phase();
        phase.setActive(active);
        phase.setName(name);
        phase.setTournaments(tournaments);
        phase.setConfrontations(confrontations);
        phase.setSec(sec);
        return phaseRepository.save(phase);
    }

    public Phase setNotActiveByConfrontation(Confrontation confrontation){
        confrontation.getPhase().setActive(false);
        return phaseRepository.save(confrontation.getPhase());
    }
}
