package com.geopago.torneo.service;

import com.geopago.torneo.Util.Constants;
import com.geopago.torneo.converter.TournamentsConverter;
import com.geopago.torneo.dto.*;
import com.geopago.torneo.handler.ResponseStatusExceptionCustom;
import com.geopago.torneo.model.*;
import com.geopago.torneo.repository.PhaseRepository;
import com.geopago.torneo.repository.TournamentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class TournamentsService {
    @Autowired
    private TournamentsRepository tournamentsRepository;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private ConfrontationService confrontationService;
    @Autowired
    private PhaseService phaseService;
    @Autowired
    private TournamentsConverter tournamentsConverter;
    @Autowired
    private PhaseRepository phaseRepository;

    public TournamentsDTO playTournament(List<FeminineDTO> players) {
        Objects.requireNonNull(players);
        List<FeminineDTO> playersControl= players.stream().filter(p->p.getSex().equals(Sex.M)).collect(Collectors.toList());
        if(playersControl!=null && playersControl.size()>0)
            throw ResponseStatusExceptionCustom.BadRequest("come wrong sex");

        double levelMax = checkSizePlayers(players.size());

        List<Player> playerResponses = players.stream()
                .filter(Objects::nonNull)
                .map(p -> playerService.create(p))
                .collect(Collectors.toList());


        return addTournaments(createAllConfrontation(playerResponses), levelMax);

    }

    public TournamentsDTO playTournamentMasculine(List<MasculineDTO> players) {
        Objects.requireNonNull(players);
        List<MasculineDTO> playersControl= players.stream().filter(p->p.getSex().equals(Sex.F)).collect(Collectors.toList());
        if(playersControl!=null && playersControl.size()>0)
            throw ResponseStatusExceptionCustom.BadRequest("come wrong sex");

        double levelMax = checkSizePlayers(players.size());

        List<Player> playerResponses = players.stream()
                .filter(Objects::nonNull)
                .map(p -> playerService.create(p))
                .collect(Collectors.toList());


        return addTournaments(createAllConfrontation(playerResponses), levelMax);

    }

    public TournamentsDTO addTournaments(List<Confrontation> confrontations, Double levelMax) {
        Tournaments tournaments = new Tournaments();
        List<Phase> phases = new ArrayList<>();
        for (int i = 0; i < levelMax; i++) {
            if (i == 0)
                phases.add(phaseService.addPhase(true, genName(levelMax.intValue() - (i + 1)), tournaments, confrontations, i));
            else
                phases.add(phaseService.addPhase(false, genName(levelMax.intValue() - (i + 1)), tournaments, new ArrayList<>(), i));

        }
        tournaments.setPhases(phases);
        return tournamentsConverter.convertToModel(tournamentsRepository.save(tournaments));
    }

    protected List<Confrontation> createAllConfrontation(List<Player> playerResponses) {
        AtomicReference<Player> aux = new AtomicReference<>(null);
        List<Confrontation> confrontations = new ArrayList<>();
        playerResponses.stream().forEach(p -> {
            if (aux.get() == null)
                aux.set(p);
            else {
                //confrontations.add(confrontationService.createConfrontation(new CreateConfrontation(p, aux.get())));
                confrontations.add(confrontationService.createConfrontation(createConfrontationAux(p, aux.get())));
                aux.set(null);
            }
        });

        return confrontations;
    }

    protected String genName(Integer i) {
        return Constants.ronda.length >= i ? Constants.ronda[i] : i + "avo";
    }

    public TournamentsDTO runTournaments(Long idTournaments) {
        Tournaments tournaments = tournamentsRepository.findById(idTournaments).orElseThrow(() -> ResponseStatusExceptionCustom.NotFound("Tournament not found by id: " + idTournaments));
        List<Phase> phasesInitial = tournamentsRepository.findTournamentGetConfrontation(idTournaments).orElseThrow(() -> ResponseStatusExceptionCustom.NotFound("Confrontation or Tournament not found"));
        Phase phase = phasesInitial.get(0);
        phase.setActive(false);
        phaseRepository.saveAndFlush(phase);
        //phasesInitial = null;    //

        List<Player> players = new ArrayList<>();
        List<Player> finalPlayers = players;
        List<Confrontation> confrontations = phase.getConfrontations();
        for (int j = 0; j < confrontations.size(); j++) {
                 finalPlayers.add(confrontationService.winner(confrontations.get(j))) ;
        }

        List<Confrontation> constantNewStage = createAllConfrontation(finalPlayers);
        for (int i = 0; i < tournaments.getPhases().size()-1; i++) {
            int sec = phasesInitial.get(0).getSec() + 1;
            phasesInitial = tournamentsRepository.findPhaseByTournamentAndSecGetConfrontation(tournaments.getId(), sec).orElseThrow(() -> ResponseStatusExceptionCustom.NotFound("Not found"));
            phasesInitial.get(0).setConfrontations(constantNewStage);
            players = phasesInitial.get(0).getConfrontations().stream().map(c -> confrontationService.winner(c)).collect(Collectors.toList());
            constantNewStage = createAllConfrontation(players);
        }

        return tournamentsConverter.convertToModel(tournamentsRepository.save(tournaments));

    }

    protected CreateConfrontation createConfrontationAux(Player idPlayer1, Player idPlayer2) {
              CreateConfrontation createConfrontation=    new CreateConfrontation();
              createConfrontation.setIdPlayer1(idPlayer1.getId());
              createConfrontation.setIdPlayer2(idPlayer2.getId());
              return createConfrontation;
    }

    protected double checkSizePlayers(int size){
        double levelMax = Math.sqrt(size);
        int entero= (int) levelMax;
        if((levelMax-entero)>0){
            throw ResponseStatusExceptionCustom.NotFound("");
        }
        return levelMax;
    }
}
