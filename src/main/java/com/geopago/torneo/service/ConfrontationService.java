package com.geopago.torneo.service;

import com.geopago.torneo.converter.ConfrontationConverter;
import com.geopago.torneo.dto.ConfrontationResponse;
import com.geopago.torneo.dto.CreateConfrontation;
import com.geopago.torneo.handler.ResponseStatusExceptionCustom;
import com.geopago.torneo.model.Confrontation;
import com.geopago.torneo.model.Feminine;
import com.geopago.torneo.model.Masculine;
import com.geopago.torneo.model.Player;
import com.geopago.torneo.repository.ConfrontationRepository;
import com.geopago.torneo.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Random;

@Service
public class ConfrontationService {
    @Autowired
    private ConfrontationRepository confrontationRepository;
    @Autowired
    private ConfrontationConverter confrontationConverter;
    @Autowired
    private PlayerRepository playerRepository;

    public ConfrontationResponse add(CreateConfrontation createConfrontation) {
        Objects.requireNonNull(createConfrontation);
        return confrontationConverter.convert(this.createConfrontation(createConfrontation), ConfrontationResponse.class);
    }

    protected Confrontation createConfrontation(CreateConfrontation createConfrontation) {
        Confrontation confrontation = new Confrontation();
        confrontation.setPlayer1(playerRepository.findById(createConfrontation.getIdPlayer1()).orElseThrow(() -> ResponseStatusExceptionCustom.NotFound("jugador no encontrado")));
        confrontation.setPlayer2(playerRepository.findById(createConfrontation.getIdPlayer2()).orElseThrow(() -> ResponseStatusExceptionCustom.NotFound("jugador no encontrado")));
        confrontation.setLuckPlayer1(getRandomNumber(100));
        confrontation.setLuckPlayer2(100);
        return confrontationRepository.save(confrontation);
    }

    public Player play_a_match(Confrontation confrontation) {
        return winner(confrontation);
    }

    public List<ConfrontationResponse> getAll() {
        return confrontationConverter.getAllPayerResponse(confrontationRepository.findAll());
    }

    public Player winner(Confrontation confrontation) {
        Objects.requireNonNull(confrontation);
        Player player=null;
        if (confrontation.getPlayer1() instanceof Feminine)
            player= winnerFemenine((Feminine) confrontation.getPlayer1(), (Feminine) confrontation.getPlayer2(), confrontation.getLuckPlayer1(), confrontation.getLuckPlayer2());
        else player=winnerMasculine((Masculine) confrontation.getPlayer1(), (Masculine) confrontation.getPlayer2(), confrontation.getLuckPlayer1(), confrontation.getLuckPlayer2());
        confrontation.setIdWinner(player.getId());
        confrontationRepository.saveAndFlush(confrontation);
        return player;
    }

    protected Player winnerFemenine(Feminine player1, Feminine player2, int luckPlayer1, int luckPlayer2) {
        int resu = player1.getAbility() + luckPlayer1 - player1.getReactionTime();
        int resu2 = player2.getAbility() + luckPlayer2 - player2.getReactionTime();
        return resu > resu2 ? player1 : player2;
    }

    protected Player winnerMasculine(Masculine player1, Masculine player2, int luckPlayer1, int luckPlayer2) {
        int resu = player1.getAbility() + luckPlayer1 + player1.getStrength() + player1.getVelocityOfDisplacement();
        int resu2 = player2.getAbility() + luckPlayer2 + player2.getStrength() + player2.getVelocityOfDisplacement();
        return resu > resu2 ? player1 : player2;
    }

    public int getRandomNumber(Integer to) {
        Random random = new Random();
        int aux = random.nextInt(to);
        return aux == 0 ? 1 : aux;
    }
}
