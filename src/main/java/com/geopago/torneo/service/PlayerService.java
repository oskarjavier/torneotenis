package com.geopago.torneo.service;

import com.geopago.torneo.converter.FeminineConverter;
import com.geopago.torneo.converter.MasculineConverter;
import com.geopago.torneo.converter.PlayerConverter;
import com.geopago.torneo.dto.CreatePlayer;
import com.geopago.torneo.dto.FeminineDTO;
import com.geopago.torneo.dto.MasculineDTO;
import com.geopago.torneo.dto.PlayerResponse;
import com.geopago.torneo.model.Player;
import com.geopago.torneo.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PlayerConverter playerConverter;
    @Autowired
    private FeminineConverter feminineConverter;
    @Autowired
    private MasculineConverter masculineConverter;


    public PlayerResponse addPlayer(CreatePlayer newPlayer) {
        return masculineConverter.convert(create(newPlayer), PlayerResponse.class);
    }
    public Player create(CreatePlayer newPlayer){
        Objects.requireNonNull(newPlayer);
        if (newPlayer instanceof FeminineDTO)
            return playerRepository.save(feminineConverter.convertToEntity((FeminineDTO) newPlayer));
        return playerRepository.save(masculineConverter.convertToEntity((MasculineDTO) newPlayer));
    }

    public List<PlayerResponse> getAll(){
        return playerConverter.getAllPayerResponse(playerRepository.findAll());
    }
}
