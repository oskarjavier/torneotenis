package com.geopago.torneo.dto;

import com.geopago.torneo.model.Sex;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CreatePlayer {
    @NotNull(message = "El nombre no debe ser NULL")
    private String name;
    @Min(value = 0, message = "El valor minimo permitido para la habilidad es de 0 (Cero)")
    @Max(value = 100, message = "El valor maximo permitido para la habilidad es de 100")
    private int ability;
    @NotNull
    private Sex sex;

    public CreatePlayer() {
    }

    public CreatePlayer(String name, int ability, Sex sex) {
        this.name = name;
        this.ability = ability;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAbility() {
        return ability;
    }

    public void setAbility(int ability) {
        this.ability = ability;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}