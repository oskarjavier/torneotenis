package com.geopago.torneo.dto;


public class MasculineDTO extends CreatePlayer {
    private int strength;
    private int velocityOfDisplacement;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getVelocityOfDisplacement() {
        return velocityOfDisplacement;
    }

    public void setVelocityOfDisplacement(int velocityOfDisplacement) {
        this.velocityOfDisplacement = velocityOfDisplacement;
    }
}
