package com.geopago.torneo.dto;


import com.geopago.torneo.model.Player;

public class CreateConfrontation {
    private Long idPlayer1;
    private Long idPlayer2;
   /* public CreateConfrontation() {
    }
*/
    /*public CreateConfrontation(Player idPlayer1, Player idPlayer2) {
        this.idPlayer1 = idPlayer1.getId();
        this.idPlayer2 = idPlayer2.getId();
    }*/

    public Long getIdPlayer1() {
        return idPlayer1;
    }

    public void setIdPlayer1(Long idPlayer1) {
        this.idPlayer1 = idPlayer1;
    }

    public Long getIdPlayer2() {
        return idPlayer2;
    }

    public void setIdPlayer2(Long idPlayer2) {
        this.idPlayer2 = idPlayer2;
    }

}
