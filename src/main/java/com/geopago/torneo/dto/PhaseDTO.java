package com.geopago.torneo.dto;

import java.util.List;

public class PhaseDTO {
    private Long id;
    //private TournamentsDTO tournaments;
    private String name;
    private Boolean active;
    private Integer sec;
    private List<ConfrontationResponse> confrontations ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*public TournamentsDTO getTournaments() {
        return tournaments;
    }

    public void setTournaments(TournamentsDTO tournaments) {
        this.tournaments = tournaments;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<ConfrontationResponse> getConfrontations() {
        return confrontations;
    }

    public void setConfrontations(List<ConfrontationResponse> confrontations) {
        this.confrontations = confrontations;
    }

    public Integer getSec() {
        return sec;
    }

    public void setSec(Integer sec) {
        this.sec = sec;
    }
}
