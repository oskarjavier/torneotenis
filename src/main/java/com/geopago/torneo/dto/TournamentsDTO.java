package com.geopago.torneo.dto;

import java.util.List;

public class TournamentsDTO {
    private Long id;
    private List<PhaseDTO> phases;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<PhaseDTO> getPhases() {
        return phases;
    }

    public void setPhases(List<PhaseDTO> phases) {
        this.phases = phases;
    }
}
