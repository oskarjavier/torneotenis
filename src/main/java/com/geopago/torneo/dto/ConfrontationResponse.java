package com.geopago.torneo.dto;

import com.geopago.torneo.model.Player;

public class ConfrontationResponse /*extends CreateConfrontation*/ {
    private long id;
    private int luckPlayer1;
    private int luckPlayer2;
    private Long idWinner;
    private PlayerResponse player1;
    private PlayerResponse player2;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getLuckPlayer1() {
        return luckPlayer1;
    }

    public void setLuckPlayer1(int luckPlayer1) {
        this.luckPlayer1 = luckPlayer1;
    }

    public int getLuckPlayer2() {
        return luckPlayer2;
    }

    public void setLuckPlayer2(int luckPlayer2) {
        this.luckPlayer2 = luckPlayer2;
    }

    public Long getIdWinner() {
        return idWinner;
    }

    public void setIdWinner(Long idWinner) {
        this.idWinner = idWinner;
    }

    public PlayerResponse getPlayer1() {
        return player1;
    }

    public void setPlayer1(PlayerResponse player1) {
        this.player1 = player1;
    }

    public PlayerResponse getPlayer2() {
        return player2;
    }

    public void setPlayer2(PlayerResponse player2) {
        this.player2 = player2;
    }
}
