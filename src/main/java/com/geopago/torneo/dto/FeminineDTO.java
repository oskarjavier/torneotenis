package com.geopago.torneo.dto;


import com.geopago.torneo.model.Sex;

import javax.validation.constraints.NotNull;

public class FeminineDTO extends CreatePlayer{
    @NotNull
    private int  reactionTime;

    public FeminineDTO() {
    }

    public FeminineDTO(int reactionTime, String name, int ability, Sex sex) {
        super(name, ability, sex);
        this.reactionTime = reactionTime;
    }

    public int getReactionTime() {
        return reactionTime;
    }

    public void setReactionTime(int reactionTime) {
        this.reactionTime = reactionTime;
    }
}
