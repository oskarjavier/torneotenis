package com.geopago.torneo.model;

import javax.persistence.*;

@Entity
@Table(name = "Confrontation")
public class Confrontation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(updatable = true, insertable = true)
    private Player player1;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(updatable = true, insertable = true)
    private Player player2;
    @Column(name = "LUCK_PLAYER1")
    private int luckPlayer1;
    @Column(name = "LUCK_PLAYER2")
    private int luckPlayer2;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(updatable = true, insertable = true, name = "phase_id")
    private Phase phase;

    @Column(name = "ID_WINNER")
    private Long idWinner;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int getLuckPlayer1() {
        return luckPlayer1;
    }

    public void setLuckPlayer1(int luckPlayer1) {
        this.luckPlayer1 = luckPlayer1;
    }

    public int getLuckPlayer2() {
        return luckPlayer2;
    }

    public void setLuckPlayer2(int luckPlayer2) {
        this.luckPlayer2 = luckPlayer2;
    }

    public Long getIdWinner() {
        return idWinner;
    }

    public void setIdWinner(Long idWinner) {
        this.idWinner = idWinner;
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }
}
