package com.geopago.torneo.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="Masculine")
public class Masculine extends Player{
    @Column(name = "STRENGTH")
    private int strength;
    @Column(name = "VELOCITY_OF_DISPLACEMENT")
    private int velocityOfDisplacement;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getVelocityOfDisplacement() {
        return velocityOfDisplacement;
    }

    public void setVelocityOfDisplacement(int velocityOfDisplacement) {
        this.velocityOfDisplacement = velocityOfDisplacement;
    }
}
