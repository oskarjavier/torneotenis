package com.geopago.torneo.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PHASE")
public class Phase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "phase_id", nullable = true)
    private List<Confrontation> confrontations ;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(updatable = true, insertable = true)
    private Tournaments tournaments;
    @Column(name = "NAME")
    private String name;

    @Column(name = "ACTIVE")
    private Boolean active;
    @Column(name = "SEC")
    private Integer sec;

    public List<Confrontation> getConfrontations() {
        return confrontations;
    }

    public void setConfrontations(List<Confrontation> confrontations) {
        this.confrontations = confrontations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Tournaments getTournaments() {
        return tournaments;
    }

    public void setTournaments(Tournaments tournaments) {
        this.tournaments = tournaments;
    }

    public Integer getSec() {
        return sec;
    }

    public void setSec(Integer sec) {
        this.sec = sec;
    }
}
