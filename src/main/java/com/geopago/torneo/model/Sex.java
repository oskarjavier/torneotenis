package com.geopago.torneo.model;

public enum Sex {
    M, F
}
