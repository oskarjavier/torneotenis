package com.geopago.torneo.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="Feminine")
public class Feminine extends Player{
    @Column(name = "REACTION_TIME")
    private int  reactionTime;

    public int getReactionTime() {
        return reactionTime;
    }

    public void setReactionTime(int reactionTime) {
        this.reactionTime = reactionTime;
    }
}
