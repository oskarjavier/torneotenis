package com.geopago.torneo.config;


import com.geopago.torneo.dto.ConfrontationResponse;
import com.geopago.torneo.model.Confrontation;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MapperConfig {

    @Bean
    public ModelMapper getModelMapper(){
        ModelMapper modelMapper=new ModelMapper();

        /*PropertyMap<Confrontation, ConfrontationResponse> entityToDtoUserResponseMapping = new PropertyMap<Confrontation, ConfrontationResponse>() {
            @Override
            protected void configure() {
                map().setIdPlayer1(source.getPlayer1().getId());
                map().setIdPlayer2(source.getPlayer2().getId());
            }
        };*/

        modelMapper.getConfiguration().setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);
        modelMapper.getConfiguration().setFieldMatchingEnabled(true);
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        //modelMapper.addMappings(entityToDtoUserResponseMapping);
        return modelMapper;
    }

}
