package com.geopago.torneo.converter;

import com.geopago.torneo.dto.CreatePlayer;
import com.geopago.torneo.dto.FeminineDTO;
import com.geopago.torneo.model.Feminine;
import com.geopago.torneo.model.Player;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FeminineConverter implements Converter<Feminine, FeminineDTO> {
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public FeminineDTO convertToModel(Feminine entity) {
        return modelMapper.map(entity, FeminineDTO.class);
    }

    @Override
    public Feminine convertToEntity(FeminineDTO model) {
        return modelMapper.map(model, Feminine.class);
    }

    @Override
    public <T> T convert(Object o, Class<T> toClass) {
        return modelMapper.map(o, toClass);
    }
}
