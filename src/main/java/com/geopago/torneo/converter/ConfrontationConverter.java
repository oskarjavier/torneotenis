package com.geopago.torneo.converter;

import com.geopago.torneo.dto.ConfrontationResponse;
import com.geopago.torneo.dto.CreateConfrontation;
import com.geopago.torneo.dto.FeminineDTO;
import com.geopago.torneo.dto.PlayerResponse;
import com.geopago.torneo.model.Confrontation;
import com.geopago.torneo.model.Feminine;
import com.geopago.torneo.model.Player;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ConfrontationConverter implements Converter<Confrontation, CreateConfrontation> {
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public CreateConfrontation convertToModel(Confrontation entity) {
        return modelMapper.map(entity, CreateConfrontation.class);
    }

    @Override
    public Confrontation convertToEntity(CreateConfrontation model) {
        return modelMapper.map(model, Confrontation.class);
    }

    @Override
    public <T> T convert(Object o, Class<T> toClass) {
        return modelMapper.map(o, toClass);
    }

    public List<ConfrontationResponse> getAllPayerResponse(List<Confrontation> confrontations){
        return confrontations.stream().map(p->convert(p, ConfrontationResponse.class)).collect(Collectors.toList());
    }
}
