package com.geopago.torneo.converter;

public interface Converter <E, M>{

    M convertToModel(E entity);
    E convertToEntity(M model);
    <T> T convert(Object o, Class<T> toClass);
}
