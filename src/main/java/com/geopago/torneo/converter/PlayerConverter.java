package com.geopago.torneo.converter;

import com.geopago.torneo.dto.CreatePlayer;
import com.geopago.torneo.dto.PlayerResponse;
import com.geopago.torneo.model.Player;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PlayerConverter implements Converter<Player, CreatePlayer> {
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public CreatePlayer convertToModel(Player entity) {
        return modelMapper.map(entity, CreatePlayer.class);
    }

    @Override
    public Player convertToEntity(CreatePlayer model) {
        return modelMapper.map(model, Player.class);
    }

    @Override
    public <T> T convert(Object o, Class<T> toClass) {
        return modelMapper.map(o, toClass);
    }

    public List<PlayerResponse> getAllPayerResponse(List<Player> players){
        return players.stream().map(p->convert(p, PlayerResponse.class)).collect(Collectors.toList());
    }
}
