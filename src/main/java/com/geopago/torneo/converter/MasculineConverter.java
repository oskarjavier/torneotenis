package com.geopago.torneo.converter;

import com.geopago.torneo.dto.FeminineDTO;
import com.geopago.torneo.dto.MasculineDTO;
import com.geopago.torneo.model.Feminine;
import com.geopago.torneo.model.Masculine;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MasculineConverter implements Converter<Masculine, MasculineDTO> {
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public MasculineDTO convertToModel(Masculine entity) {
        return modelMapper.map(entity, MasculineDTO.class);
    }

    @Override
    public Masculine convertToEntity(MasculineDTO model) {
        return modelMapper.map(model, Masculine.class);
    }

    @Override
    public <T> T convert(Object o, Class<T> toClass) {
        return modelMapper.map(o, toClass);
    }
}
