package com.geopago.torneo.converter;

import com.geopago.torneo.dto.TournamentsDTO;
import com.geopago.torneo.model.Tournaments;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TournamentsConverter implements Converter<Tournaments, TournamentsDTO>{

    @Autowired
    private ModelMapper modelMapper;
    @Override
    public TournamentsDTO convertToModel(Tournaments entity) {
        return modelMapper.map(entity, TournamentsDTO.class);
    }

    @Override
    public Tournaments convertToEntity(TournamentsDTO model) {
        return modelMapper.map(model, Tournaments.class);
    }

    @Override
    public <T> T convert(Object o, Class<T> toClass) {
        return null;
    }
}
