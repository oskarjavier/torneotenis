package com.geopago.torneo.controller;

import com.geopago.torneo.dto.FeminineDTO;
import com.geopago.torneo.dto.MasculineDTO;
import com.geopago.torneo.dto.TournamentsDTO;
import com.geopago.torneo.service.TournamentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/torneo/tournaments")
@RestController
public class TournamentsController {
    @Autowired
    private TournamentsService tournamentsService;

    @PostMapping(value = "/feminine", produces = {"application/json"})
    public ResponseEntity<TournamentsDTO> addTournament(@Validated @RequestBody List<FeminineDTO> players){
        return new ResponseEntity<>(tournamentsService.playTournament(players), HttpStatus.OK);
    }

    @PostMapping(value = "/masculine", produces = {"application/json"})
    public ResponseEntity<TournamentsDTO> addTournamentMasculine(@Validated @RequestBody List<MasculineDTO> players){
        return new ResponseEntity<>(tournamentsService.playTournamentMasculine(players), HttpStatus.OK);
    }

    @PostMapping(value = "/play/{idTournament}")
    public ResponseEntity runTournaments(@PathVariable("idTournament") Long idTournament){
        return new ResponseEntity(tournamentsService.runTournaments(idTournament), HttpStatus.OK);
    }
}
