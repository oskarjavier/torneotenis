package com.geopago.torneo.controller;

import com.geopago.torneo.dto.FeminineDTO;
import com.geopago.torneo.dto.MasculineDTO;
import com.geopago.torneo.dto.PlayerResponse;
import com.geopago.torneo.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/torneo/player")
@RestController
public class PlayerController {
    @Autowired
    private PlayerService playerService;

    @PostMapping(value = "/sex=feminine", produces = {"application/json"})
    public ResponseEntity<PlayerResponse> add(@Validated @RequestBody(required = true) FeminineDTO player){
        return new ResponseEntity<>(playerService.addPlayer(player), HttpStatus.OK);
    }

    @PostMapping(value = "/sex=masculine", produces = {"application/json"})
    public ResponseEntity<PlayerResponse> add(@Validated @RequestBody MasculineDTO player){
        return new ResponseEntity<>(playerService.addPlayer(player), HttpStatus.OK);
    }

    @GetMapping(value = "/", produces = {"application/json"})
    public ResponseEntity<List<PlayerResponse>> getAll(){
        return new ResponseEntity<>(playerService.getAll(), HttpStatus.OK);
    }
}
