package com.geopago.torneo.controller;

import com.geopago.torneo.dto.ConfrontationResponse;
import com.geopago.torneo.dto.CreateConfrontation;
import com.geopago.torneo.service.ConfrontationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/torneo/confrontation")
@RestController
public class ConfrontationController {
    @Autowired
    private ConfrontationService confrontationService;

    @PostMapping(value = "/", produces = {"application/json"})
    public ResponseEntity<ConfrontationResponse> add(@Validated @RequestBody(required = true) CreateConfrontation createConfrontation) {
        return new ResponseEntity<>(confrontationService.add(createConfrontation), HttpStatus.OK);
    }

    @GetMapping(value = "/", produces = {"application/json"})
    public ResponseEntity<List<ConfrontationResponse>> getAll() {
        return new ResponseEntity<>(confrontationService.getAll(), HttpStatus.OK);
    }
}
