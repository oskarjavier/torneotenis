package com.geopago.torneo.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;


    public class ResponseStatusExceptionCustom extends ResponseStatusException {
    private static final Logger logger = LoggerFactory.getLogger(ResponseStatusExceptionCustom.class);

    public ResponseStatusExceptionCustom(HttpStatus status) {
        super(status);
    }

    public ResponseStatusExceptionCustom(HttpStatus status, String reason) {
        super(status, reason);
        this.log(status, reason, null);
    }

    public ResponseStatusExceptionCustom(HttpStatus status, String reason, Throwable cause) {
        super(status, reason, cause);
        this.log(status, reason, null);
    }

    private void log(HttpStatus status, String reason, Exception exception) {
        logger.error(status + ": " + reason + "\n", exception);
    }


    public static ResponseStatusException NotFound(String reason){
        return new ResponseStatusExceptionCustom(HttpStatus.NOT_FOUND, reason);
    }

    public static ResponseStatusException BadRequest(String reason){
        return new ResponseStatusExceptionCustom(HttpStatus.BAD_REQUEST, reason);
    }
}
