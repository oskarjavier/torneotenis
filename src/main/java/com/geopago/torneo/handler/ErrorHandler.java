package com.geopago.torneo.handler;

import com.geopago.torneo.dto.ApiError;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    public ErrorHandler() {
        super();
    }

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleBadRequest(final ConstraintViolationException ex, final WebRequest request) {
        logger.error("400 Status Code", ex);
        return new ResponseEntity<Object>(new ApiError(HttpStatus.CONFLICT, ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ DataIntegrityViolationException.class })
    public ResponseEntity<Object> handleBadRequest(final DataIntegrityViolationException ex, final WebRequest request) {
        //logger.error("400 Status Code", ex.getMessage());
        logger.error("400 Status Code", ex);
        return new ResponseEntity<Object>(new ApiError(HttpStatus.CONFLICT, ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ InvalidDataAccessApiUsageException.class, DataAccessException.class })
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request) {
        logger.error("409 Status Code", ex);
        final ApiError apiError = new ApiError(HttpStatus.CONFLICT, ex.getLocalizedMessage());
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.CONFLICT);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.error("400 Status Code", ex);
        return new ResponseEntity<Object>(new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        logger.error("400 Status Code", ex);
        return new ResponseEntity<Object>(new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { EntityNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(final RuntimeException ex, final WebRequest request) {
        logger.error("404 Status Code", ex);

        return new ResponseEntity<Object>(new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ IllegalArgumentException.class })
    public ResponseEntity<Object> handleInternalIllegalA(final RuntimeException ex, final WebRequest request) {
        logger.error("400 Status Code", ex);
        return new ResponseEntity<Object>(new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ NullPointerException.class, IllegalStateException.class, ArithmeticException.class })
    public ResponseEntity<Object> handleInternal(final RuntimeException ex, final WebRequest request) {
        logger.error("500 Status Code", ex);
        return new ResponseEntity<Object>(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
